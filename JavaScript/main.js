alert('Hello World'); 
console.log('Hello World');
console.error('This is an error');
console.warn('This is a warning');

const name = 'Brad';
const age = 37;
const rating = 3.5;
const isCool = true;
const x = null;
const y = undefined;
let z;


console.log(typeof z);



console.log('My name is ' + name + ' and I am ' + age);
console.log(`My name is ${name} and I am ${age}`);


const s = 'Hello World';
let val;
val = s.length;
val = s.toUpperCase();
val = s.toLowerCase();
val = s.substring(0, 5);
val = s.split('');


console.log('My name is ' + name + ' and I am ' + age);
console.log(`My name is ${name} and I am ${age}`);


// String methods & properties
const s = 'Hello World';
let val;
val = s.length;
val = s.toUpperCase();
val = s.toLowerCase();
val = s.substring(0, 5);
val = s.split('');


const numbers = [1,2,3,4,5];
const fruits = ['apples', 'oranges', 'pears', 'grapes'];
console.log(numbers, fruit);

console.log(fruits[1]);
fruits[4] = 'blueberries';

fruits.push('strawberries');

fruits.unshift('mangos');
fruits.pop();
console.log(Array.isArray(fruits));
console.log(fruits.indexOf('oranges'));


// OBJECT LITERALS
const person = {
    firstName: 'John',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
      street: '50 Main st',
      city: 'Boston',
      state: 'MA'
    }
  }
  

  console.log(person.name)
  
  console.log(person.hobbies[1]);
  
  console.log(person.address.city);
  
  person.email = 'jdoe@gmail.com';
  
  const todos = [
    {
      id: 1,
      text: 'Take out trash',
      isComplete: false
    },
    {
      id: 2,
      text: 'Dinner with wife',
      isComplete: false
    },
    {
      id: 3,
      text: 'Meeting with boss',
      isComplete: true
    }
  ];
  
  console.log(todos[1].text);
  
  console.log(JSON.stringify(todos));



// LOOPS

for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`);
  }
  
let i = 0
while(i <= 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
  }
  

  for(let i = 0; i < todos.length; i++){
    console.log(` Todo ${i + 1}: ${todos[i].text}`);
  }
  
  for(let todo of todos) {
    console.log(todo.text);
  }


  // CONDITIONALS

const x = 30;

if(x === 10) {
  console.log('x is 10');
} else if(x > 10) {
  console.log('x is greater than 10');
} else {
  console.log('x is less than 10')
}

// Switch
color = 'blue';

switch(color) {
  case 'red':
    console.log('color is red');
  case 'blue':
    console.log('color is blue');
  default:  
    console.log('color is not red or blue')
}

const z = color === 'red' ? 10 : 20;


// FUNCTIONS
function greet(greeting = 'Hello', name) {
    if(!name) {
      // console.log(greeting);
      return greeting;
    } else {
      // console.log(`${greeting} ${name}`);
      return `${greeting} ${name}`;
    }
  }
  
  
  const greet = (greeting = 'Hello', name = 'There') => `${greeting} ${name}`;
  console.log(greet('Hi'));


  // OOP

function Person(firstName, lastName, dob) {
    
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob); // Set to actual date object using Date constructor
  }
  
  // Get Birth Year
  Person.prototype.getBirthYear = function () {
    return this.dob.getFullYear();
  }
  
  // Get Full Name
  Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`
  }
  
  
  // Instantiate an object from the class
  const person1 = new Person('John', 'Doe', '7-8-80');
  const person2 = new Person('Steve', 'Smith', '8-2-90');
  
  console.log(person2);
  

  
  
  
  // Built in constructors
  const name = new String('Kevin');
  console.log(typeof name); // Shows 'Object'
  const num = new Number(5);
  console.log(typeof num); // Shows 'Object'
  
  
  // ES6 CLASSES
  class Person {
    constructor(firstName, lastName, dob) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.dob = new Date(dob);
    }
  
    // Get Birth Year
    getBirthYear() {
      return this.dob.getFullYear();
    }
  
    // Get Full Name
    getFullName() {
      return `${this.firstName} ${this.lastName}`
    }
  }
  
  const person1 = new Person('John', 'Doe', '7-8-80');
  console.log(person1.getBirthYear());
  
// ELEMENT SELECTORS

console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
// Multiple Element Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));


// MANIPULATING THE DOM
const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

const btn = document.querySelector('.btn');
// btn.style.background = 'red';

// EVENTS

// Mouse Event
btn.addEventListener('click', e => {
    e.preventDefault();
    console.log(e.target.className);
    document.getElementById('my-form').style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
  });
  
  // Keyboard Event
  const nameInput = document.querySelector('#name');
  nameInput.addEventListener('input', e => {
    document.querySelector('.container').append(nameInput.value);
  });


  // USER FORM SCRIPT

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}
